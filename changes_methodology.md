## Changes in the methodology compared to the original method (Hirschi et. al 2013)

- GR_A11 Ecomorphology: recorded for the whole of Switzerland and zero values equated with NA;
individual cantons such as Zug and St. Gallen have no mapped values according to the modular
concept of the federal government, Valais and Graubünden only the main valleys, Ticino and Fribourg not
completely (BAFU 2009).
- GR_A14 Renaturation and revitalization data: not centrally available at the time of data collection.
centrally available, therefore values in GR were equated with NA.
- GR_A15 Dilution ratio at wastewater treatment plants (WWTPs) for discharges: Zero values
equal to NA.
- GR_A20 Water flow: use WASTA (2013) with hydroelectric power plants (> 300 kW) under
Federal control and dams serving hydroelectricity (Dam, as of 2013).
- GR_C05 Synoptic hazard maps: are cantonally managed at the time of data collection,
Values in GR are marked with a 5 so that the systematics in the decision tree is not affected.
is affected.
- Water quality (GR_A15, GR_A16, GR_A17, GR_A18, GR_A19): for the evaluation of the function type.
A Nature, it is important whether the median of the five values is less than or equal to 3 in total. This
evaluation is based on the decision tree for evaluating GR (Hirschi et al. 2013:22).
Therefore, an evaluation of the station data is made where critical and possible river segments
with poor quality (median less than 3) exist. Only two longer and one short
sections in Switzerland receive a lower median than 3 for water quality.
- LR_B06 Visibility: For 99 percent of the river segments (30,733 of 31,062) in the canton of
Bern (2015 reduced version), the landscape area is considered to be visible. Due to this high number
of sections, a large number of viewpoints in the layer of Swisstopo and
the computation time and computability in ArcGIS, the landscape area is classified as generally viewable
(equal to 1).
16 Method
Additional indicators were added (see Appendix B.2):
- LR_A21 Dissection
- LR_A22 Forest areas
- LR_B03 Hiking trails
- LR_B10 Residential and vacation homes
- LR_B11 Tourist infrastructure
- LR_C01 Landfill
- LR_C03 Infrastructure
- LR_C05 Industry
- LR_C06 Agricultural land
Not to be added, although present to some extent:
- LR_B06 Cultural assets of national importance: here, too, the calculability of the visibility analysis is
for the whole of Switzerland is limited
- LR_A15 Legally binding protection and land use planning: the individual river sections are not
clearly designated, i.e. no geodata exist
The following data are also not supplemented, as they are cantonal data:
- LR_A10 Cantonal nature reserves
- LR_A16 Forest reserves
- LR_A17 Cantonal inventories and contractually protected areas

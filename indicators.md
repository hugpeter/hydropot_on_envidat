# RIVER SPACE 

## Type A Regulating ecosystem functions

Type A indicators capture supporting (nutrient cycling, soil formation, primary production, etc) and regulating ecosystem functions (climate regulation, water purification, etc). 

| Component | ID | Indicator | Scale | Value | Data source | Data source (German description) |
|---|---|---|---|---|---|---|
| Priority area | RS_A01 | Mires and mire landscapes of special beauty and national importance | Present | 9 | Federal Inventory of mires (Federal Office for the Environment, FOEN) | Bundesinventar der Moorlandschaften (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  | RS_A02 | Upland and intermediate mires of national importance | Present | 9 | Federal inventory of upland and intermediate mires(FOEN) | Bundesinventar der Hoch- und Übergangsmoore (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  | RS_A03 | Lowland mires of national importance | Present | 9 | Federal Inventory of lowland mires(FOEN) | Bundesinventar der Flachmoore (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  | RS_A04 | Floodplain areas of national importance | Present | 9 | Federal Inventory of Floodplain Areas (FOEN) | Bundesinventar der Auengebiete (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  | RS_A05 | Amphibian spawning areas of national importance | Present | 9 | Federal Inventory of Amphibian spawning Areas (FOEN) | Bundesinventar der Amphibienlaichgebiete (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  | RS_A06 | Protected areas for waterfowl and migratory birds of international and national importance | present | 9 | Waterbird and migratory bird reserves (FOEN) | Wasser- und Zugvogelreservate (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  | RS_A07 | River reaches of national importance | Present | 9 | Grayling and Common nase populations (FOEN); crayfish populations (CSCF). | Äschen- und Nasenpopulationen (BAFU); Flusskrebspopulationen (CSCF) |
|  |  |  | Absent | 1 |  |  |
|  | RS_A08 | River reaches with occurrence of the endangered fish species Roi du Doubs, Sofie, Savetta, Marble trout or Common nase. | Present | 9 | Occurrence of Roi du Doubs, Sofie, Savetta, Marble trout, Common nase (CSCF). | Vorkommen von Roi du Doubs, Sofie, Savetta, Marmorforelle, Nase (CSCF) |
|  |  |  | Absent | 1 |  |  |
|  | RS_A09 | Existing national parks | Present | 9 | Swiss National Park (FOEN) | Schweizerischer Nationalpark (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  | RS_A10 | Protected areas according to the Ordinance on Compensation for losses in hydropower. | Present | 9 | ‘VAEW’ areas (BAFU) | VAEW-Gebiete (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  |  |  |  |  |  |  |
| River morphology | RS_A11 | River state | Natural | 5 | Section classification from ecomorphology level F (BAFU) | Abschnittsklassifizierung aus Ökomorphologie Stufe F (BAFU) |
|  |  |  | Little affected | 4 |  |  |
|  |  |  | Severely affected | 3 |  |  |
|  |  |  | UNatural | 2 |  |  |
|  |  |  | Culverted? | 1 |  |  |
|  | RS_A12 | CoNectivity: Artificial cascades? | No fall per 100 m | 5 | Cascades from ecomorphology level F (BAFU) | Abstürze aus Ökomorphologie Stufe F (BAFU) |
|  |  |  | >0 to ≤1 falls per 100 m | 4 |  |  |
|  |  |  | >1 to ≤2 falls per 100 m | 3 |  |  |
|  |  |  | >2 to ≤3 falls per 100 m | 2 |  |  |
|  |  |  | >3 crashes per 100 m | 1 |  |  |
|  | RS_A13 | State of the bed load balance | No structure per 100 m | 5 | Structures from ecomorphology level F (BAFU) | Bauwerke aus Ökomorphologie Stufe F (BAFU) |
|  |  |  | >0 to ≤1 structures per 100 m | 4 |  |  |
|  |  |  | >1 to ≤2 structures per 100 m | 3 |  |  |
|  |  |  | >2 to ≤3 structures per 100 m | 2 |  |  |
|  |  |  | >3 structures per 100m | 1 |  |  |
|  |  |  |  |  |  |  |
|  | RS_A14 | Existing and planned river restoration projects | Present | 5 | Measures to improve river morphology, ecology and coNectivity from hydraulic enginEring projects of the cantonal civil enginEring and fisheries departments | Massnahmen zur Verbesserung der Gewässermorphologie, –ökologie und der Vernetzung aus Wasserbauprojekten der kantonalen Fachstellen für Tiefbau und Fischerei |
|  |  |  | Absent | 1 |  |  |
|  |  |  |  |  |  |  |
| Water quality | RS_A15 | Dilution ratios for wastewater treatment plant (WWTP) discharges | <25 % | 5 | Wastewater treatment plant (BAFU) | Abwasserreinigungsanlage (BAFU) |
|  |  |  | 25 -50 % | 3 |  |  |
|  |  |  | >50 % | 1 |  |  |
|  | RS_A16 | Orthophosphate PO4 -P [mg P/l] | Very good: <0.02 | 5 | Quality of surface waters from the National River Monitoring and Survey Programme (NADUF) of the FOEN or/and water quality measurements of the Cantonal Departments for Water | Qualität der Oberflächengewässer aus der Nationalen Daueruntersuchung der Fliessgewässer (NADUF) des BAFU oder/und Wasserqualitätsmessungen der Kantonalen Abteilung für Wasser |
|  |  |  | Good: 0.02 to <0.04 | 4 |  |  |
|  |  |  | Moderate: 0.04 to <0.06 | 3 |  |  |
|  |  |  | Unsatisfactory: 0.06 to <0.08 | 2 |  |  |
|  |  |  | Bad: ≥0.08 | 1 |  |  |
|  | RS_A17 | Nitrate NO3 [mg N/l] | Very good: <1.5 | 5 | Quality of surface waters from the National River Monitoring and Survey Programme (NADUF) of the FOEN or/and water quality measurements of the Cantonal Department for Water | Qualität der Oberflächengewässer aus der Nationalen Daueruntersuchung der Fliessgewässer (NADUF) des BAFU oder/und Wasserqualitätsmessungen der Kantonalen Abteilung für Wasser |
|  |  |  | Good: 1.5 to < 5.6 | 4 |  |  |
|  |  |  | Moderate: 5.6 to <8.4 | 3 |  |  |
|  |  |  | Unsatisfactory: 8.4 to <11.2 | 2 |  |  |
|  |  |  | Bad: ≥11.2 | 1 |  |  |
|  | RS_A18 | Ammonium NH4 [mg N/l] | Very good: <0.04 | 5 | Quality of surface waters from the National River Monitoring and Survey Programme (NADUF) of the FOEN or/and water quality measurements of the Cantonal Department for Water | Qualität der Oberflächengewässer aus der Nationalen Daueruntersuchung der Fliessgewässer (NADUF) des BAFU oder/und Wasserqualitätsmessungen der Kantonalen Abteilung für Wasser |
|  |  |  | Good: 0.04 to <0.2 | 4 |  |  |
|  |  |  | Moderate: 0.2 to <0.3 | 3 |  |  |
|  |  |  | Unsatisfactory: 0.3 to <0.4 | 2 |  |  |
|  |  |  | Bad: ≥0.4 | 1 |  |  |
|  | RS_A19 | Dissolved organic carbon DOC [mg C/l] | Very good: <2.0 | 5 | Quality of surface waters from the National River Monitoring and Survey Programme (NADUF) of the FOEN or/and water quality measurements of the Cantonal Department for Water | Qualität der Oberflächengewässer aus der Nationalen Daueruntersuchung der Fliessgewässer (NADUF) des BAFU oder/und Wasserqualitätsmessungen der Kantonalen Abteilung für Wasser |
|  |  |  | Good: 2.0 to < 4.0 | 4 |  |  |
|  |  |  | Moderate: 4.0 to <6.0 | 3 |  |  |
|  |  |  | Unsatisfactory: 6.0 to <8.0 | 2 |  |  |
|  |  |  | Bad: ≥8.0 | 1 |  |  |
|  |  |  |  |  |  |  |
| River flow | RS_A20 | Changes in streamriver flow due to hydropower (hydropeaking) | No changes due to hydropower | 5 | Hydropower plants statistics, WASTA (Swiss Federal Office of Energy) | Laufkraftwerke, Umwälzwerke, Speicherkraftwerke und Pumpspeicherkraftwerke aus der Statistik der Wasserkraftanlagen (WASTA) des BfE und Wasserkraftanlagen der Kantone |
|  |  |  | 1 run-of-river power plant present within 1000m upstream | 2 |  |  |
|  |  |  | ≥2 run-of-river power plants present within 1,000m upstream or/and one (pump-) storage power plant present within 20km upstream | 1 |  |  |
|  | RS_A21 | Environmental flow reaches: Share of reduced streamflow volume [%]. | No environmental flow reach | 5 | Environmental flow reaches by Bernhard Wehrli (EAWAG) | Restwasserstrecken von Bernhard Wehrli (EAWAG) |
|  |  |  | Purple line: > 90 | 4 |  |  |
|  |  |  | GrEn track: 50 - 90 | 3 |  |  |
|  |  |  | Orange route: 10 - 50 | 2 |  |  |
|  |  |  | Red line or gray line: < 10 % or assumption that no water is abstracted | 1 |  |  |
|  |  |  |  |  |  |  |
|  |  |  |  |  |  |  |


## Type B Cultural ecosystem functions

Type B indicators capture cultural ecosystem functions (aesthetics, recreation, spirituality, pedagogy, etc). 


| Component | ID | Indicator | Scale | Value | Data source | Data source (German description) |
|---|---|---|---|---|---|---|
| Priority area | RS_B01 | Existing nature experience park | Present | 9 | Nature experience parks (FOEN) | Naturerlebnispärke (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  |  |  |  |  |  |  |
| Accessibility | RS_B02 | Accessibility & Visibility: Trail Network Density (TND) of trails, roads, rail lines, and public transportation lines per river space [m/m2 x 100]. | Very easily accessible and visible: TND> 1 | 5 | Road, railroad, hiking trail network and public transport network from VEKTOR25 and swisstopo's swissTLM3D or traffic routes from the cantonal departments of transport. | Strassen-, Eisenbahn-, Wanderwegnetz und Netz des öffentlichen Verkehrs aus VEKTOR25 und swissTLM3Dder swisstopo oder Verkehrswege der kantonalen Abteilungen für Verkehr |
|  |  |  | Easily accessible and visible: TND ≤ 1 and > 0.75 | 4 |  |  |
|  |  |  | Averagely well accessible and visible: TND ≤ 0.75 and > 0.5 | 3 |  |  |
|  |  |  | Poorly accessible and visible: TND ≤ 0.5 and > 0.25 | 2 |  |  |
|  |  |  | Very poorly or not accessible and visible: TND ≤ 0.25 | 1 |  |  |
|  |  |  |  |  |  |  |
| Experience character | RS_B03 | Waterfalls, caves, gorges, swimming opportunities and/or other landscape aesthetic features. | Present | 5 | Waterfalls, caves, canyons and swimming from Map+ by TYDAC (www.mapplus.ch) | Wasserfälle, Höhlen, Schluchten und Schwimmen aus Map+ von TYDAC (www.mapplus.ch) |
|  |  |  | Absent | 1 |  |  |
|  |  |  |  |  |  |  |
| Leisure and sports | RS_B04 | Angling (average catch of trout per km in 2005) | >150 piece | 5 | Fish catch statistics Switzerland (BAFU) | Fischfangstatistik Schweiz (BAFU) |
|  |  |  | >100< to ≤150 pieces | 4 |  |  |
|  |  |  | >50 to ≤100 pieces | 3 |  |  |
|  |  |  | > 0 to ≤50 pieces | 2 |  |  |
|  |  |  | None | 1 |  |  |
|  | RS_B05 | Water sports: river rafting / canoe / kayak / canyoning | Present | 5 | Canoe country (http://map.wanderland.ch), paddling routes (www.rivermap.ch), canyoning routes (www.schlucht.ch, canyoning guides by BruNer & Bétrisey (2001), Baumgartner, BruNer, & ZimmermaN (2010), cantonal data. | Kanuland (http://map.wanderland.ch), Paddelstrecken (www.rivermap.ch), Canyioningstrecken (www.schlucht.ch, Canyoning-Führer von BruNer & Bétrisey (2001), Baumgartner, BruNer, & ZimmermaN (2010), kantonale Daten |
|  |  |  | Absent | 1 |  |  |
|  |  |  |  |  |  |  |
|  |  |  |  |  |  |  |


## Type C Providing ecosystem functions

Type C indicators capture providing ecosystem functions (food, fresh water, fuel, etc).

| Component | ID | Indicator | Scale | Value | Data source | Data source (German description) |
|---|---|---|---|---|---|---|
| Priority space | RS_C01 | Groundwater protection zones S1 & S2 | Present | 9 | Groundwater protection map (FOEN) | Grundwasserschutzkarte (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  | RS_C02 | Groundwater protection areas | Present | 9 | Groundwater protection areas (FOEN) | Grundwasserschutzareale (BAFU) |
|  |  |  | Absent | 1 |  |  |
|  |  |  |  |  |  |  |
| Resource utilisation | RS_C03 | Water abstraction for hydropower or other uses | Abstraction volumeWithdrawal quantity >50 % of Q347 | 5 | Environmental flow map (BAFU) | Restwasserkarte (BAFU) |
|  |  |  | Abstraction volume <50 % of Q347 or unknown, but harmless from an environmental point of view | 4 |  |  |
|  |  |  | further abstractions (abstractions volumequantity unknown) | 3 |  |  |
|  |  |  | No water abstractions | 1 |  |  |
|  |  |  |  |  |  |  |
| Hydropower potential | RS_C04 | Specific hydropower potential [kW/m] | Very high potential: >3.0 kW/m | 5 | Specific hydropower potential (WaterGisWeb AG) | Spezifische Leistung (WaterGisWeb AG) |
|  |  |  | High potential: >1.0 to ≤3.0 kW/m | 4 |  |  |
|  |  |  | Average potential: >0.3 to ≤1.0 kW/m | 3 |  |  |
|  |  |  | Low potential: >0.1 to ≤0.3 kW/m | 2 |  |  |
|  |  |  | Very low potential: 0 to ≤0.1 kW/m | 1 |  |  |
|  |  |  |  |  |  |  |
| Risk disposition | RS_C05 | Synoptic hazard map: Rockfall, avalanche, landslide, flood and collapse hazards | No danger | 5 | Synoptic hazard map from the cantonal natural hazard maps or hazard information map | Synoptische Gefahrenkarte aus den kantonalen Naturgefahrenkarten oder Gefahrenhinweiskarte |
|  |  |  | Area of residual? hazard predominates (yellow shaded zone). | 4 |  |  |
|  |  |  | Area of low hazard predominates (yellow zone) | 3 |  |  |
|  |  |  | Area of medium hazard predominates (blue zone) | 2 |  |  |
|  |  |  | Area of significant hazard predominates (red zone). | 1 |  |  |
|  |  |  |  |  |  |  |
|  |  |  |  |  |  |  |


# Surrrounding Landscape space

## Type A Regulating ecosystem functions

Type A indicators capture supporting (nutrient cycling, soil formation, primary production, etc) and regulating ecosystem functions (climate regulation, water purification, etc). 
| Component | ID | Indicator | Scale | Value | Data source | Data source (German description) |
|---|---|---|---|---|---|---|
| Protected landscapes | SLS_A01 | Mires and mire landscapes of special beauty and national importance | Present | 1 | Federal Inventory of mires (Federal Office for the Environment, FOEN) | Bundesinventar der Moorlandschaften (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A02 | Upland and intermediate mires of national importance | Present | 1 | Federal inventory of upland and intermediate mires(FOEN) | Bundesinventar der Hoch- und Übergangsmoore (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A03 | Lowland mires of national importance | Present | 1 | Federal Inventory of lowland mires(FOEN) | Bundesinventar der Flachmoore (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A04 | Amphibian spawning areas of national importance | Present | 1 | Federal Inventory of Amphibian Spawning Areas (FOEN) | Bundesinventar der Amphibienlaichgebiete (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A05 | Dry meadows and pastures of national importance | Present | 1 | Federal Inventory of Dry Meadows and Pastures (FOEN) | Bundesinventar der Trockenwiesen und -weiden (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A06 | *Landscapes and natural monuments of national importance | Present | 1 | Federal Inventory of Landscapes and Natural Monuments (FOEN) | Bundesinventar der Landschaften und Naturdenkmäler (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A07 | Existing and planned national parks | Present | 1 | Swiss National Parks (FOEN) | Schweizer Nationalpärke (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A08 | Existing and planned regional nature parks | Present | 1 | Regional nature parks (FOEN) | Regionale Naturpärke (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A09 | Federal hunting territories | Present | 1 | Federal hunting zones (FOEN) | Eidgenössische Jagdbanngebiete (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A10 | Cantonal nature reserves | Present | 1 | Cantonal nature reserves of the cantonal departments for nature protection | Kantonale Naturschutzgebiete der kantonalen Abteilungen für Naturschutz |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A11 | Ramsar sites | Present | 1 | Ramsar sites (FOEN) | Ramsar-Gebiete (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A12 | Smaragd areas | Present | 1 | Emerald areas (FOEN) | Smaragd-Gebiete (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A13 | UNESCO Biosphere Reserves | Present | 1 | UNESCO Biosphere Reserves (FOEN) | UNESCO Biosphärenreservate (BAFU) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A14 | UNESCO World Heritage Sites | Present | 1 | UNESCO World Heritage Sites (UNESCO) | UNESCO Weltnaturerbe-Gebiete (UNESCO) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A15 | Legally binding protection and utilization planning | Present | 1 | Protection and land use planning according to the Water Protection Act (BAFU 2009) | Schutz- und Nutzungsplanung nach Gewässerschutzgesetz (BAFU 2009) |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A16 | Forest Reserves | Present | 1 | Forest reserves of the cantonal departments of forest | Waldreservate der kantonalen Abteilungen für Wald |
|  |  |  | Absent | 0 |  |  |
|  | SLS_A17 | Cantonal inventories and contractual nature conservation: rare forest communities, protected botanical and geological objects, etc., as well as contractually protected dry sites, wetlands, etc. | Present | 1 | Cantonal inventories and contractually protected areas of the cantonal departments for nature conservation. | Kantonale Inventare und vertraglich geschützte Flächen der kantonalen Abteilungen für Naturschutz |
|  |  |  | Absent | 0 |  |  |
|  |  |  |  |  |  |  |
| Biodiversity | SLS_A18 | BDM; modeled species richness: vascular plants [species/km2 ] (modeled prediction; vascular plants). | large number: ≥243 to ≤365 | 1 | Biodiversity monitoring of vascular plants (WSL) | Biodiversitätsmonitoring vaskulärer Pflanzen (WSL) |
|  |  |  | low-medium number of species: ≤242 | 0 |  |  |
|  | SLS_A19 | Red List species, priority species, or species with APN protection status (fauna): mammals, fish, crustaceans, cyclostomes, amphibians, reptiles, mollusks, insects, and brEding birds. | A Critically Endangered (CR), Endangered (EN), Vulnerable (VU), or a Class 1, 2, 3 priority species or federally protected species (APN) Class 2, 2a, 3 present. | 1 | Red List Species, Priority Species, APN Species (CSCF and Swiss Volgewarte Sempach) | Rote Liste Arten, prioritäre Arten, APN Arten (CSCF und Schweizerische Volgewarte Sempach) |
|  |  |  | No Red List, priority or APN protected species present. | 0 |  |  |
|  |  |  |  |  |  |  |
| Connectivity | SLS_A20 | Streams/lakes or wetlands according to REN. | Core area, dispersal area, continuum, or ecological corridor present. | 1 | National Ecological Network (FOEN) | Nationales ökologisches Netzwerk (BAFU) |
|  |  |  | No networking element present (island) | 0 |  |  |
|  | SLS_A21 | Dissection of the landscape | No fragmentation of the landscape area by roads and railways | 1 | Road, railroad and public transport network from VEKTOR25 and swissTLM3D of swisstopo or traffic routes of the cantonal departments for transport | Strassen-, Eisenbahnnetz und Netz des öffentlichen Verkehrs aus VEKTOR25 und swissTLM3D der swisstopo oder Verkehrswege der kantonalen Abteilungen für Verkehr |
|  |  |  | Roads and rails cut up the landscape space | 0 |  |  |
|  |  |  |  |  |  |  |
| Land use | SLS_A22 | Forest areas | Forest makes up more than 30% of the landscape area | 1 | Forest from VECTOR25 of swisstopo | Wald aus VECTOR25 der swisstopo |
|  |  |  | Forest makes up less than 30% of the landscape area | 0 |  |  |
|  |  |  |  |  |  |  |
| *Criterion can lead to exclusion, i.e. further clarifications are necessary (e.g. inclusion of the protection decision) |  |  |  |  |  |  |
|  |  |  |  |  |  |  |


## Type B Cultural ecosystem functions 
Type B indicators capture cultural ecosystem functions (aesthetics, recreation, spirituality, pedagogy, etc).


| Component | ID | Indicator | Scale | Value | Data source | Data source (German description) |
|---|---|---|---|---|---|---|
| Landscape | SLS_B01 | Existing and planned nature experience parks | present | 1 | Nature experience parks (FOEN) | Naturerlebnispärke (BAFU) |
|  |  |  | absent | 0 |  |  |
|  | SLS_B02 | *UNESCO World Heritage Sites | present | 1 | World Heritage Sites (UNESCO) | Weltkulturerbe-Gebiete (UNESCO) |
|  |  |  | Absent | 0 |  |  |
|  |  |  |  |  |  |  |
| Leisure and sports | SLS_B03 | marked hiking trails | Hiking trails present | 1 | Cantonal hiking route network or hiking trails from swissTLM3D of swisstopo | Kantonales Wanderroutennetz oder Wanderwege von swissTLM3D der swisstopo |
|  |  |  | no hiking trails present | 0 |  |  |
|  | SLS_B04 | marked cycle routes | Bike lanes present | 1 | Cantonal bike route network or Veloland, Mountainbikeland and Skatingland of the Switzerland Mobile Foundation. | Kantonales Veloroutennetz oder Veloland, Mountainbikeland und Skatingland der Stiftung Schweiz Mobil |
|  |  |  | no bike lanes present | 0 |  |  |
|  | SLS_B05 | Infrastructure for recreational use: restaurants, bars, cafes, hotels, cabins, camping, botanical gardens, fireplaces, playgrounds, benches, zoos, nature trails, outdoor pools, swimming areas, rope parks, Vita trails, horseback riding trails, etc. | Infrastructure for recreational use present | 1 | Bars, cafés, hotels, restaurants, fireplaces and playgrounds from search.ch (http://map.search.ch) and points of interest from Map+ (www.mapplus.ch) as well as data from cantonal structure plans | Bars, Cafés, Hotels, Restaurants, Feuerstellen und Spielplätze aus search.ch (http://map.search.ch) und Points of Interest aus Map+ (www.mapplus.ch) sowie Daten aus kantonalen Richtplänen |
|  |  |  | No infrastructure for recreational use present | 0 |  |  |
|  |  |  |  |  |  |  |
| Experience character | SLS_B06 | Visibility | Landscape area is visible from viewpoints | 1 | Viewpoints from cantonal structure plans, viewpoints from Map+ (www.mapplus.ch), view towers from Vektor25 of swisstopo | Aussichtslagen kantonaler Richtpläne, Aussichtspunkte aus Map+ (www.mapplus.ch), Aussichtstürme aus Vektor25 der swisstopo |
|  |  |  | Landscape area is not visible from vantage points | 0 |  |  |
|  |  |  |  |  |  |  |
| Cultural and historical landscape inventories | SLS_B07 | Inventory of Historic Transport Routes in Switzerland (IVS) | A historic traffic run or at least one element accompanying the path is present | 1 | Inventory of historical traffic routes in Switzerland (Astra) | Inventar der historischen Verkehrswege der Schweiz (Astra) |
|  |  |  | No historical traffic routes or path accompanying elements present | 0 |  |  |
|  | SLS_B08 | Inventory of Swiss Sites Worthy of Protection (ISOS) | a townscape worthy of protection visible from the landscape area | 1 | Inventory of sites worthy of protection in Switzerland (BAFU) | Inventar der schützenswerten Ortsbilder der Schweiz (BAFU) |
|  |  |  | No sites worthy of protection visible from the landscape area | 0 |  |  |
|  | SLS_B09 | Swiss Inventory of Cultural Assets of National Importance (KGS) | a cultural asset visible from the landscape space | 1 | Swiss Inventory of Cultural Property of National Importance (FOCP) | Schweizerisches Inventar der Kulturgüter von nationaler Bedeutung (BABS) |
|  |  |  | No cultural assets visible from the landscape area | 0 |  |  |
|  |  |  |  |  |  |  |
| Land use | SLS_B10 | Residential and vacation homes | Residential and cottage zones account for more than 10% of the landscape area | 1 | Residential and vacation home zones from cantonal zoning plans or residential zones from building zones Switzerland (ARE) | Wohn- und Ferienhauszonen aus kantonalen Zonenplänen oder Wohnzonen aus Bauzonen Schweiz (ARE) |
|  |  |  | Residential and cottage zones make up less than 10% of the landscape area | 0 |  |  |
|  |  |  |  |  |  |  |
| Tourism | SLS_B11 | tourist infrastructure: cable and gondola lifts, ski slopes, cross-country skiing trails, golf courses, boat lines, toboggan runs, horse racing tracks, etc. | Infrastructures for tourism present | 1 | Ski slopes, cross-country ski trails from the cantonal zoning and structure plans; boat lines from the cantonal public transport department; cable cars and ski lifts from Vektor25 of swisstopo. | Skipisten, Loipen aus den kantonalen Zonen- und Richtplänen; Schiffslinien der kantonalen Abteilung für öffentlichen Verkehr; Seilbahnen und Skilifte aus Vektor25 der swisstopo |
|  |  |  | no infrastructures for tourism present | 0 |  |  |
|  |  |  |  |  |  |  |
|  |  |  |  |  |  |  |
| Noise protection | SLS_B12 | Railroad noise during the day | more than 50 % of the landscape area has a noise pollution of ≤55 dB | 1 | SonBase - the GIS noise database of Switzerland (FOEN) | SonBase - die GIS-Lärmdatenbank der Schweiz (BAFU) |
|  |  |  | more than 50 % of the landscape area has a noise pollution of >55 dB | 0 |  |  |
|  | SLS_B13 | Road noise during the day | more than 50 % of the landscape area has a noise pollution of ≤55 dB | 1 | SonBase - the GIS noise database of Switzerland (FOEN) | SonBase - die GIS-Lärmdatenbank der Schweiz (BAFU) |
|  |  |  | more than 50 % of the landscape area has a noise pollution of >55 dB | 0 |  |  |
|  | SLS_B14 | Aircraft noise during the day | more than 50 % of the landscape area has a noise pollution of ≤55 dB | 1 | SonBase - the GIS noise database of Switzerland (FOEN) | SonBase - die GIS-Lärmdatenbank der Schweiz (BAFU) |
|  |  |  | more than 50 % of the landscape area has a noise pollution of >55 dB | 0 |  |  |
|  |  |  |  |  |  |  |
| *Criterion can lead to exclusion, i.e. further clarifications are necessary (e.g. inclusion of the protection decision) |  |  |  |  |  |  |
|  |  |  |  |  |  |  |

## Type C Providing ecosystem functions 
Type C indicators capture providing ecosystem functions (food, fresh water, fuel, etc). 

| Component | ID | Indicator | Scale | Value | Data source | Data source (German description) |
|---|---|---|---|---|---|---|
| Raw material utilization | SLS_C01 | Mining and landfill | Mining and landfill sites are present | 1 | Mining and/or deposition zones from the cantonal zoning plans | Abbau- und/oder Ablagerungszonen aus den kantonalen Zonenplänen |
|  |  |  | No mining and landfill sites present | 0 |  |  |
|  | SLS_C02 | Wastewater treatment plant (ARA) | present | 1 | Wastewater treatment plants (BAFU) | Abwasserreinigungsanlagen (BAFU) |
|  |  |  | absent | 0 |  |  |
|  |  |  |  |  |  |  |
| Soil suitability | SLS_C03 | Technical infrastructure: construction zones or other (tourist) buildings/facilities that indicate existing technical infrastructure (e.g. cable car, ski lift, roads, railroad, power lines). | technical infrastructures present | 1 | Building zones of the cantonal zoning plans or building zones of the ARE; cable cars, ski lifts, road, railroad network and public transport network from VEKTOR25 and swissTLM3D of swisstopo or traffic routes of the cantonal departments of transport. | Bauzonen der kantonalen Zonenpläne oder Bauzonen des ARE; Seilbahnen, Skilifte, Strassen-, Eisenbahnnetz und Netz des öffentlichen Verkehrs aus VEKTOR25 und swissTLM3D der swisstopo oder Verkehrswege der kantonalen Abteilungen für Verkehr |
|  |  |  | No technical infrastructures present | 0 |  |  |
|  | SLS_C04 | Hydropower municipality (tax revenues from hydropower) | Landscape area is located within a hydropower community | 1 | Daniel Spreng resp. Mr. Balmer from ETH Zurich | Daniel Spreng resp. Herr Balmer von der ETH Zürich |
|  |  |  | Landscape area lies outside hydropower community | 0 |  |  |
|  |  |  |  |  |  |  |
| Land use | SLS_C05 | Industry and commerce: mixed zones, urban core zone, work zone, public use zone and military use zone. | Industry and commerce account for more than 10% of the landscape area | 1 | Mixed zones, urban core zone, work zone, zone for public use, zone for military use, etc. from cantonal zoning plans or work zone, mixed zone, zones for public use, traffic zones and other building zones from Building Zones Switzerland of the ARE. | Mischzonen, Kernzone städtisch, Arbeitszone, Zone für öffentliche Nutzung, Zone für militärische Nutzung, etc. aus kantonalen Zonenplänen oder Arbeitszone, Mischzone, Zonen für öffentliche Nutzungen, Verkehrszonen und weitere Bauzonen aus Bauzonen Schweiz des ARE |
|  |  |  | Industry and commerce account for less than 10% of the landscape area | 0 |  |  |
|  | SLS_C06 | Agricultural land | Agricultural land makes up more than 30% of the landscape area | 1 | Remaining area from VECTOR25 of swisstopo | Übriges Gebiet aus VECTOR25 der swisstopo |
|  |  |  | Agricultural land accounts for less than 30% of the landscape area | 0 |  |  |

# HYDROpot_integral: a spatial dataset and tool to simultaneously assess hydropower potential and ecological potential of the Swiss river network

### Carla Laub<sup>1</sup>, Tobias Wechsler <sup>1,2</sup>, Dorothea Hug Peter <sup>2</sup>, Massimiliano Zappa<sup>2</sup>, Rolf Weingartner<sup>1</sup>

1: Institute of Geography (GIUB) and Oeschger Centre for Climate Change Research (OCCR) University of Bern (tobias.wechsler@giub.unibe.ch)
2: Swiss Federal Institute for Forest, Snow and Landscape Research WSL (dorothea.hug@wsl.ch)

## Introduction
The steadily growing demand for energy and the simultaneous pursuit of decarbonisation are increasing interest in the expansion of renewable energies worldwide. In Switzerland, various funding projects have been launched to promote technologies in the field of renewable energies and their application as quickly as possible. With the introduction of a funding instrument in 2009, the number of projects submitted to produce renewable energies increased rapidly. The applications for small hydropower plants (≤ 10 MW) were correspondingly numerous. However, the assessment of the environmental impact and its comparison with hydropower importance is still not standardized.
To provide a basis for decision-making, a methodology was developed to determine the overall hydropower potential of a region. A detailed assessment of each river reach, and the systematic and holistic assessment of small hydropower projects at a regional scale are combined here. The assessment of a river reach is conducted at the river space (i.e., the river and adjacent areas) and at the surrounding landscape level. The HYDROpot_integral methodology was developed as part of Carol Hemund's dissertation (2012) at the University of Bern. It allows the evaluation of river reaches holistically, regarding ecological, social, economic and cultural criteria. As a second part of the overall project, the theoretical hydropower (or hydraulic) potential was calculated for the entire river network, which complemnets the spatial assessment. In particular, it is possible to classify river reaches into those that are more suitable for hydropower production (=”use”) and those that are more suitable for protection.

## Material and method
The HYDROpot_Integral method was developed and tested on the basis of cantonal and national data (Hirschi et al. 2013). The method relies on 73 geodata sets. This holistic assessment is the key element of the entire assessment procedure. Its aim is to quantify the importance of the ecosystem functions in terms of services. The river network (GWN07) is divided into reaches of about 450m and for each reach two study units are defined. The river space (RS) records the ecosystem functions of the water body and the nearby riparian area. The length of the RS is 315 m on average in Switzerland and a maximum of 450 m, whereas the width is based on the FOEN definition (BWG 2001: 18f) and varies between 7-107 m. The surrounding landscape (SLS) is the second survey unit that records the ecosystem functions of the surrounding area over a range of 21 m to 321 m. The SLS is calculated over three times the RS width. The length of the SLS is identical to the length of the RS. The ecosystem functions are divided into three types: regulating (service A), cultural (service B) and provisioning (service C) functions. Accordingly, the assessment of the functions is divided into three parts and three values are assigned to each river reach. The more functions there are and the greater their performance, the higher these values are and the more important the corresponding functions are. Hence, these values quantify the importance of the ecosystem functions and the ecological, cultural and economic ecosystem services of each river reach. The concatenation of ecosystem services results in a value (ABC) that can occur in 125 different versions due to the chosen five-level value scale; i.e. each digit of the three-digit number sequence can be assigned a value between 1 and 5. Each of the 125 combinations, and thus each river reach, has its own characteristics determined by the assessments of the three function types. To record the suitability, the combinations are ranked according to their ecological, cultural and economic ecosystem services. These rules mean that the combination that is most suitable for hydropower production at minimum cost in terms of ecological and cultural ecosystem services and has a high economic potential is ranked first; rank 125 indicates the highest ecological and cultural ecosystem services and the lowest economic services and is therefore most suitable for protection. A river reach that is excluded from hydropower use due to legislation, a so-called priority reach, is given rank 126 from the outset and specially marked. A more detailed description of the methods can be found in Hirschi et al. 2013 [Link]. The dataset presented here presents the latest state of the HYDROpot_integral methodology applied at the national level. Only national data that is easily accessible was used in the preparation of the dataset. The cantonal data, such as renaturation and revitalization, would have to be requested by each canton individually and was excluded here. The nationwide value synthesis was made with R.
A list of data sources can be found here [link to text file]
A list of all parameters can be downloaded here [link to PDF and text files]

## Dataset description
Data is presented as a single shapefile. It contains the river network and all assessment results obtained with HYDROpot_Integral. 

## Literature


### Project BFE
-  Hemund, C. et al. (2012): Erhebung des Kleinwasserkraftpotentials der Schweiz - Ermittlung des theoretischen Potentials und Methodik zu dessen ganzheitlicher Beurteilung. Bundesamt für Energie.

[PDF](https://www.aramis.admin.ch/Dokument.aspx?DocumentID=65306)

- WaterGISWeb AG (2008): Erhebung des Kleinwasserkraftpotentials der Schweiz. Ermittlung des hydroelektrischen Potentials für Kleinwasserkraftwerke in der Schweiz. (Jahresbericht).

[Online publication](https://map.geo.admin.ch/?lang=de&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe&layers=ch.swisstopo.zeitreihen,ch.bfs.gebaeude_wohnungs_register,ch.bav.haltestellen-oev,ch.swisstopo.swisstlm3d-wanderwege,ch.astra.wanderland-sperrungen_umleitungen,ch.bfe.kleinwasserkraftpotentiale&layers_opacity=1,1,1,0.8,0.8,1&layers_visibility=false,false,false,false,false,true&layers_timestamp=18641231,,,,,&catalogNodes=614,687,720)

[BFE Webpage](https://www.bfe.admin.ch/bfe/de/home/versorgung/statistik-und-geodaten/geoinformation/geodaten/wasser/kleinwasserkraftwerkpotentiale-der-schweizer-gewaesser.html)

-  Pfaundler, M.; Zappa, M. (2006): Die mittleren Abflüsse über die ganze Schweiz. Ein optimierter Datensatz im 500×500m Raster. Herausgegeben von BAFU und WSL. BAFU; WSL.

[PDF](https://www.dora.lib4ri.ch/wsl/islandora/object/wsl%3A7073/datastream/PDF/Pfaundler-2006-Die_mittleren_Abfl%C3%BCsse_%C3%BCber_die-%28published_version%29.pdf)



### Dissertation

- Hemund, C. (2012): Methodik zur ganzheitlichen Beurteilung des Kleinwasserkraftpotentials in der Schweiz. Inauguraldissertation, Leiter: Prof. Dr. R. Weingartner. Universität Bern, Geographisches Institut.

[PDF](http://www.hydrologie.unibe.ch/download/518_Hemund.pdf)

### Case studies

- Hirschi, Joëlle; Wechsler, Tobias; Rey, Emmanuel; Weingartner, Rolf (2013): Ganzheitliche Beurteilung des Wasserkraftpotentials schweizerischer Fliessgewässer ‒ Handbuch zur GIS-gestützten Anwendung der Methode HYDROpot_integral. Bericht im Auftrag des BfE. Publikation Gewässerkunde Nr. 608, Bern.

[PDF](https://boris.unibe.ch/47942/)


### Master projects

- Studer, D. (2010): Beurteilung von Landschaftsteilräumen. Methodik zur Abgrenzung, Aufnahme, Auswertung und Darstellung des IST-Zustands von Landschaftsteilräumen am Beispiel des Einzugsgebiets der Lütschine. Masterarbeit, Publikation Gewässerkunde Nr. 458. Universität Bern, Geographisches Institut.

- Baumgartner, I. (2010): Methode zur ganzheitlichen Beurteilung von Gewässerräumen. Entwickelt und getestet am Beispiel des Einzugsgebiets der Lütschine im Berner Oberland. Masterarbeit, Gewässerkunde Nr. 461. Universität Bern, Geographisches Institut.

- Hirschi, J. (2012): Ganzheitliche Beurteilung von Gewässer- und Landschaftsräumen. Eine GIS-basierte Methodik zur Identifizierung von Gewässerräumen für eine potentielle Kleinwasserkraftnutzung in der Schweiz. Masterarbeit, Gewässerkunde Nr. 512. Universität Bern, Geographisches Institut.

- Laub, C. (2016): Räumlich differenzierte Erhebung des ganzheitlich beurteilten Wasserkraftpotentials der Schweiz. Masterarbeit, Gewässerkunde Nr. 663. Universität Bern, Geographisches Institut.

 

